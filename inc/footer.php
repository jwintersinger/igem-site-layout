    <div id="footer">
      <div class="container">
        <div class="row">
          <div class="col-span-12">
            <ul id="footer-navbar">
              <?php require(dirname(__FILE__).'/full_nav.php'); ?>
            </ul>

            <a href="http://igem.org/"><div id="igem-logo"></div></a>
          </div>
        </div>
      </div>
    </div>

    <!-- JavaScript plugins (requires jQuery) -->
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- Optionally enable responsive features in IE8 -->
    <script src="/bootstrap/js/respond.min.js"></script>

    <script src="/js/global.js"></script>
    <!--<script src="/js/guide.js"></script>-->
    <?php if(isset($extra_footer)) echo $extra_footer; ?>
  </body>
</html>
