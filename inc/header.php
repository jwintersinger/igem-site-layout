<!DOCTYPE html>
<html class="no-touch">
  <head>
    <title>iGEM Calgary // <?php echo $title; ?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/css/global.css" rel="stylesheet" media="screen">
    <link href="/css/global.mobile.css" rel="stylesheet" media="only screen and (max-width:600px)">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <?php if(isset($extra_header)) echo $extra_header; ?>
  </head>

  <body>
    <div id="guide">&nbsp;</div>

    <div class="header-bar minimal">
      <div id="nav-trigger"></div>
      <ul id="nav-popover">
        <?php require(dirname(__FILE__).'/full_nav.php'); ?>
      </ul>
    </div>

    <div class="header-bar full">
      <div class="secondary-navbar">
        <?php require(dirname(__FILE__).'/secondary_nav.php'); ?>
      </div>

      <div class="header-content">
        <div class="container">
          <?php require(dirname(__FILE__).'/primary_nav.php'); ?>
        </div>
      </div>
    </div>
