<?php
$title = 'Home';
$extra_header = <<<EOD
  <link href="/css/home.css" rel="stylesheet" media="screen">
  <link href="/css/home.mobile.css" rel="stylesheet" media="only screen and (max-width:600px)">
EOD;
$extra_footer = <<<EOD
  <script src="/js/home.js"></script>
EOD;

require('inc/header.php');
?>

<div id="masthead-secondary-navbar" class="secondary-navbar">
  <?php require('inc/secondary_nav.php'); ?>
</div>

<div id="masthead">
  <div class="container">
    <?php require('inc/primary_nav.php'); ?>

    <div class="row">
      <div class="col-span-12">
        <div id="message">We are using synthetic biology to build a better world.</div>
      </div>
    </div>

    <div class="row">
      <div class="col-span-12">
        <a href="#what-is-igem" id="learn-more" class="button">Learn more</a>
      </div>
    </div>
  </div>
</div>

<div id="content">
  <div class="container">
    <div class="row">
      <div class="col-span-12">
        <h1 id="what-is-igem">what is <span class="last-word">iGEM?</span></h1>
        <hr />
        <p>iGEM is an annual <span class="highlight">synthetic
        biology*</span> competition involving undergraduate students from
        around the world. Students collaborate to create synthetic biology
        projects and contribute to a library of standardized synthetic
        biology parts.</p>
        <div id="synbio-venn"></div>
        <p class="explanation"><span class="highlight">*</span> Synthetic
        biology takes systems designed by nature and combines them in novel
        ways to build useful things.</p>
      </div>
    </div>

    <div class="row">
      <div class="col-span-12">
        <h1>what are we <span class="last-word">building?</span></h1>
        <hr />
        <p>We are a team of fifteen undergraduate students at the
        University of Calgary who are constructing a system to detect
        harmful strains of <em>E. coli</em> in meat.</p>
        <div id="atom-diagram"></div>
      </div>
    </div>
  </div>
</div>

<?php require('inc/footer.php'); ?>
