<?php
$title = 'Sponsors';
$extra_header = <<<EOD
  <link href="/css/sponsors.css" rel="stylesheet" media="screen">
  <link href="/css/sponsors.mobile.css" rel="stylesheet" media="only screen and (max-width:600px)">
EOD;
$extra_footer = <<<EOD
  <script src="/js/sponsors.js"></script>
EOD;

require('../inc/header.php');
?>

<div id="content">
  <div class="container">
    <div class="row header first">
      <div class="col-span-12">
        <h1><span>Testimonials</span></h1>
      </div>
    </div>

    <div class="row" id="testimonials-row">
      <div id="testimonials" class="clearfix">
        <div class="testimonial" id="elizabeth-cannon">
          <div class="quote">
            The commitment and dedication the iGEM team has demonstrated
            serves as an inspiration to our entire campus community as they
            pursue innovation and excellence. [I] believe that they serve
            as a great example of what can be achieved through
            collaboration, creativity, and dedication.
            <div class="triangle"></div>
            <div class="left-quote"></div>
            <div class="right-quote"></div>
          </div>

          <div class="quotee">
            <div class="person">
              Elizabeth Cannon<br />
              President and Vice-Chancellor<br />
              University of Calgary
            </div>
            <a href="http://www.ucalgary.ca/"><div class="greyscale quotee-logo"></div></a>
          </div>
        </div>

        <div class="testimonial" id="jennifer-hill">
          <div class="quote">
            I am proud to be affiliated with these students, and to see the
            good they have done not only for the scientific field but for
            their university and their community. ... In their travels they
            are incredible ambassadors for the spirit and talent of their
            home city.
            <div class="triangle"></div>
            <div class="left-quote"></div>
            <div class="right-quote"></div>
          </div>

          <div class="quotee">
            <div class="person">
              Jennifer Hill<br />
              Co-Lead, geekStarter<br />
              Alberta Innovates&ndash;Technology Futures
            </div>
            <a href="http://www.albertatechfutures.ca/"><div class="greyscale quotee-logo"></div></a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-span-12">
      </div>
    </div>

    <div class="row header" id="partner-with-us">
      <div class="col-span-12">
        <h1><span>Partner with us</span></h1>
      </div>
    </div>

    <div class="row">
      <div class="col-span-4 col-push-4" id="partner-with-us">
        <p>
          We would love to talk to you about potential sponsorship
          opportunities with our team. Help us build the next great thing
          in synthetic biology, while promoting your brand to a global
          audience of science professionals. Why not reach out and say
          hello?
        </p>

        <a href="/contact/" class="button">Contact us &rarr;</a>
      </div>
    </div>

    <div class="row header">
      <div class="col-span-12">
        <h1><span>Sponsors</span></h1>
      </div>
    </div>

    <div id="sponsor-logos">
      <div class="row">
        <div class="col-span-12" id="sponsor-logos-wrapper">
          <div class="sponsor-logo" id="aitf-logo">
            <a href="http://www.albertatechfutures.ca/"><div class="greyscale image" style="background-image: url('/images/sponsor-logos/original/aitf.png'); background-size: 177px;"></div></a>
          </div>
          <div class="sponsor-logo" id="genome-alberta-logo">
            <a href="http://genomealberta.ca/"><div class="greyscale image" style="background-image: url('/images/sponsor-logos/original/genome-alberta.png'); background-size: 154px;"></div></a>
          </div>
          <div class="sponsor-logo" id="university-of-calgary-logo">
            <a href="http://ucalgary.ca/"><div class="greyscale image" style="background-image: url('/images/sponsor-logos/original/university-of-calgary.png'); background-size: 103px;"></div></a>
          </div>
          <div class="sponsor-logo" id="o-brien-centre-logo">
            <a href="http://www.ucalgary.ca/bhsc/"><div class="greyscale image" style="background-image: url('/images/sponsor-logos/original/o-brien-centre.png'); background-size: 225px;"></div></a>
          </div>
          <!--<div class="sponsor-logo" id="shell-logo">
            <a href="http://www.shell.ca/"><div class="greyscale image" style="background-image: url('/images/sponsor-logos/original/shell.png'); background-size: 88px;"></div></a>
          </div>-->
          <div class="sponsor-logo" id="biobasic-logo">
            <a href="http://www.biobasic.com/"><div class="greyscale image" style="background-image: url('/images/sponsor-logos/original/biobasic.png'); background-size: 110px;"></div></a>
          </div>
          <div class="sponsor-logo" id="kapa-biosystems-logo">
            <a href="http://www.kapabiosystems.com/"><div class="greyscale image" style="background-image: url('/images/sponsor-logos/original/kapa-biosystems.png'); background-size: 209px;"></div></a>
          </div>
          <div class="sponsor-logo" id="sarstedt-logo">
            <a href="http://www.sarstedt.com/"><div class="greyscale image" style="background-image: url('/images/sponsor-logos/original/sarstedt.png'); background-size: 175px;"></div></a>
          </div>
          <div class="sponsor-logo" id="new-england-biolabs-logo">
            <a href="http://www.neb.ca/"><div class="greyscale image" style="background-image: url('/images/sponsor-logos/original/new-england-biolabs.png'); background-size: 146px;"></div></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require('../inc/footer.php'); ?>
