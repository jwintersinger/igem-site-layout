<?php
// Taken from http://stackoverflow.com/a/8072733/1691611.
function filter_email($email) {
  $rule = array("\r" => '',
    "\n" => '',
    "\t" => '',
    '"'  => '',
    ','  => '',
    '<'  => '',
    '>'  => '',
  );

  return strtr($email, $rule);
}

function filter_name($name) {
  $rule = array("\r" => '',
    "\n" => '',
    "\t" => '',
    '"'  => "'",
    '<'  => '[',
    '>'  => ']',
  );

  return trim(strtr($name, $rule));
}

function filter_message($msg) {
  $msg = wordwrap($msg, 70);
  // PHP manual says this is necessary, but it just adds extraneous newlines to
  // the e-mail.
  //$msg = str_replace("\n", "\r\n", $msg);
  return $msg;
}

if($_SERVER['REQUEST_METHOD'] == 'POST') {
  require_once('../inc/config.php');

  $msg     = filter_message($_POST['message']);
  $subject = 'Contact from iGEM Calgary web site';
  $to      = implode(', ', $CONTACT_EMAILS);
  $from    = filter_name($_POST['name']).' <'.filter_email($_POST['email']).'>';
  if(mail($to, $subject, $msg, 'From: '.$from)) {
    header('Location: '.$_SERVER['PHP_SELF'].'?thanks');
    exit();
  } else {
    exit('Error sending e-mail.');
  }
} else {
  $title = 'Contact';
  $extra_header = <<<EOD
    <link href="/css/contact.css" rel="stylesheet" media="screen">
    <!--<link href="/css/contact.mobile.css" rel="stylesheet" media="only screen and (max-width:600px)">-->
EOD;
  $extra_footer = <<<EOD
    <!--<script src="/js/contact.js"></script>-->
EOD;

  require('../inc/header.php');
?>

<div id="content">
  <div class="container">
<?php
  if($_SERVER['QUERY_STRING'] == 'thanks') {
?>
    <div class="row">
      <div class="col-span-6 col-push-3" style="text-align: center; padding-bottom: 300px">
        <h1>Thanks!</h1>
        <p>We'll be in touch soon.</p>
      </div>
    </div>
<?php
  } else {
?>
    <div class="row header first" id="get-in-touch">
      <div class="col-span-12">
        <h1><span>Get in touch</span></h1>
      </div>
    </div>

    <div id="contact-icons">
      <a href="http://www.facebook.com/igemcalgary"><div class="contact-icon" id="facebook-icon"></div></a>
      <a href="http://www.twitter.com/igemcalgary"><div class="contact-icon" id="twitter-icon"></div></a>
    </div>

    <div class="row header" id="say-hello">
      <div class="col-span-12">
        <h1><span>Say hello</span></h1>
      </div>
    </div>

    <div class="row">
      <div class="col-span-6 col-push-3">
        <form action="" method="post">
          <input type="text" name="name" placeholder="Your name ..." />
          <input type="email" name="email" placeholder="Your e-mail ..." />
          <textarea name="message" rows="15" placeholder="Your message ..."></textarea>
          <button type="submit" class="button" id="send-message">Send message</button>
        </form>
      </div>
    </div>

<?php
  }
?>
  </div>
</div>
<?php
}
require('../inc/footer.php');
?>
